package com.javiertirado.appmenubutton

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.appmenubutton.R
import com.javiertirado.appmenubutton.database.Alumno
import com.javiertirado.appmenubutton.database.dbAlumnos
import com.squareup.picasso.Picasso
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class DbFragment : Fragment() {
    private lateinit var btnGuardar: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnBorrar: Button
    private lateinit var btnLimpiar: Button // Nuevo botón para limpiar
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var txtUrlImagen: EditText
    private lateinit var imgAlumno: ImageView
    private lateinit var db: dbAlumnos

    private val IMAGE_PICK_CODE = 1000

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        btnGuardar = view.findViewById(R.id.btnGuardar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        btnLimpiar = view.findViewById(R.id.btnLimpiar) // Inicializa el botón de limpiar
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        txtUrlImagen = view.findViewById(R.id.txtUrlImagen)
        imgAlumno = view.findViewById(R.id.imgAlumno)

        db = dbAlumnos(requireContext())

        arguments?.let {
            val alumno = it.getSerializable("mialumno") as? Alumno
            alumno?.let {
                txtNombre.setText(alumno.nombre)
                txtDomicilio.setText(alumno.domicilio)
                txtEspecialidad.setText(alumno.especialidad)
                txtMatricula.setText(alumno.matricula)
                txtUrlImagen.setText(alumno.foto)
                loadImage(alumno.foto)
            }
        }

        imgAlumno.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK).apply {
                type = "image/*"
            }
            startActivityForResult(intent, IMAGE_PICK_CODE)
        }

        btnGuardar.setOnClickListener {
            handleSaveButtonClick()
        }

        btnBuscar.setOnClickListener {
            val matricula = txtMatricula.text.toString()
            if (matricula.isEmpty()) {
                Toast.makeText(requireContext(), "Por favor, ingrese la matrícula para buscar", Toast.LENGTH_SHORT).show()
            } else {
                try {
                    db.openDataBase()
                    val alumno = db.getAlumno(matricula)
                    if (alumno.id != 0) {
                        txtNombre.setText(alumno.nombre)
                        txtDomicilio.setText(alumno.domicilio)
                        txtEspecialidad.setText(alumno.especialidad)
                        txtUrlImagen.setText(alumno.foto)
                        loadImage(alumno.foto)
                        Toast.makeText(requireContext(), "Alumno encontrado", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "La matrícula ingresada no existe", Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Error al acceder a la base de datos: ${e.message}", Toast.LENGTH_SHORT).show()
                } finally {
                    db.close()
                }
            }
        }

        btnBorrar.setOnClickListener {
            val matricula = txtMatricula.text.toString()
            if (matricula.isEmpty()) {
                Toast.makeText(requireContext(), "Por favor, ingrese la matrícula para borrar", Toast.LENGTH_SHORT).show()
            } else {
                val builder = AlertDialog.Builder(requireContext())
                builder.setMessage("¿Está seguro de que desea eliminar este alumno?")
                    .setPositiveButton("Sí") { dialog, id ->
                        try {
                            db.openDataBase()
                            val result = db.BorrarAlumno(matricula)
                            if (result > 0) {
                                Toast.makeText(requireContext(), "Alumno eliminado con éxito", Toast.LENGTH_SHORT).show()
                                limpiar()
                            } else {
                                Toast.makeText(requireContext(), "La matrícula ingresada no existe", Toast.LENGTH_SHORT).show()
                            }
                        } catch (e: Exception) {
                            Toast.makeText(requireContext(), "Error al acceder a la base de datos: ${e.message}", Toast.LENGTH_SHORT).show()
                        } finally {
                            db.close()
                        }
                    }
                    .setNegativeButton("No") { dialog, id ->
                        dialog.dismiss()
                    }
                builder.create().show()
            }
        }

        btnLimpiar.setOnClickListener {
            limpiar() // Llama al método limpiar cuando se hace clic en el botón
        }

        return view
    }

    private fun handleSaveButtonClick() {
        if (valiEmpty()) {
            Toast.makeText(requireContext(), "Por favor, ingrese toda la información", Toast.LENGTH_SHORT).show()
        } else {
            val fotoPath = txtUrlImagen.text.toString()
            val alumno = Alumno(
                matricula = txtMatricula.text.toString(),
                nombre = txtNombre.text.toString(),
                domicilio = txtDomicilio.text.toString(),
                especialidad = txtEspecialidad.text.toString(),
                foto = fotoPath
            )

            try {
                db.openDataBase()
                val existingAlumno = db.getAlumno(alumno.matricula)

                if (existingAlumno.id != 0) {
                    val result = db.ActualizarAlumno(alumno, existingAlumno.id)
                    if (result > 0) {
                        Toast.makeText(requireContext(), "Alumno actualizado con éxito", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "Error al actualizar el alumno", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    val id: Long = db.InsertarAlumno(alumno)
                    if (id > 0) {
                        Toast.makeText(requireContext(), "Alumno agregado con éxito con ID $id", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "Error al agregar el alumno", Toast.LENGTH_SHORT).show()
                    }
                }
                notifyAlumnoAdded(alumno)
                limpiar()
            } catch (e: Exception) {
                Toast.makeText(requireContext(), "Error al acceder a la base de datos: ${e.message}", Toast.LENGTH_SHORT).show()
            } finally {
                db.close()
            }
        }
    }

    private fun notifyAlumnoAdded(alumno: Alumno) {
        val targetFragment = targetFragment
        if (targetFragment is AlumnoListener) {
            (targetFragment as AlumnoListener).onAlumnoAdded(alumno)
        } else {
            Log.w("DbFragment", "Target fragment is not an instance of AlumnoListener")
        }
    }

    private fun valiEmpty(): Boolean {
        return txtNombre.text.toString().isEmpty() ||
                txtDomicilio.text.toString().isEmpty() ||
                txtMatricula.text.toString().isEmpty() ||
                txtEspecialidad.text.toString().isEmpty() ||
                txtUrlImagen.text.toString().isEmpty()
    }

    private fun limpiar() {
        Log.d("DbFragment", "Iniciando la limpieza de campos")

        txtMatricula.text.clear()
        txtNombre.text.clear()
        txtDomicilio.text.clear()
        txtEspecialidad.text.clear()
        txtUrlImagen.text.clear()

        imgAlumno.setImageResource(R.drawable.baseline_account_24)

        Log.d("DbFragment", "Campos después de la limpieza: Matricula: ${txtMatricula.text}, Nombre: ${txtNombre.text}, Domicilio: ${txtDomicilio.text}, Especialidad: ${txtEspecialidad.text}, UrlImagen: ${txtUrlImagen.text}")
    }

    private fun loadImage(imagePath: String?) {
        if (imagePath != null && imagePath.isNotEmpty()) {
            val imageFile = File(imagePath)
            if (imageFile.exists()) {
                Picasso.get().load(imageFile).into(imgAlumno)
            } else {
                imgAlumno.setImageResource(R.drawable.baseline_account_24)
            }
        } else {
            imgAlumno.setImageResource(R.drawable.baseline_account_24)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            val imageUri = data?.data
            imageUri?.let {
                try {
                    val savedImagePath = saveImageToInternalStorage(it)
                    txtUrlImagen.setText(savedImagePath)
                    loadImage(savedImagePath)
                } catch (e: IOException) {
                    Toast.makeText(requireContext(), "Error al guardar la imagen: ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun saveImageToInternalStorage(imageUri: Uri): String {
        val inputStream = requireContext().contentResolver.openInputStream(imageUri)!!
        val file = File(requireContext().filesDir, "selected_image.jpg")
        val outputStream = FileOutputStream(file)
        val buffer = ByteArray(1024)
        var bytesRead: Int
        while (inputStream.read(buffer).also { bytesRead = it } != -1) {
            outputStream.write(buffer, 0, bytesRead)
        }
        inputStream.close()
        outputStream.close()
        return file.absolutePath
    }
}
