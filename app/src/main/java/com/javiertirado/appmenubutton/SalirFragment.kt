package com.javiertirado.appmenubutton

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubutton.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.javiertirado.appmenubutton.database.Alumno
import com.javiertirado.appmenubutton.database.dbAlumnos

class SalirFragment : Fragment(), MiAdaptador.OnItemClickListener {

    private lateinit var rcvLista: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var btnNuevo: FloatingActionButton
    private lateinit var searchView: SearchView

    private lateinit var listaAlumno: ArrayList<Alumno> // Lista original de alumnos
    private lateinit var listaFiltrada: ArrayList<Alumno> // Lista filtrada para el adaptador

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_salir, container, false)

        rcvLista = view.findViewById(R.id.recId)
        btnNuevo = view.findViewById(R.id.agregarAlumno)
        searchView = view.findViewById(R.id.searchView)

        rcvLista.layoutManager = LinearLayoutManager(requireContext())

        // Obtener los alumnos desde la base de datos
        val db = dbAlumnos(requireContext())
        db.openDataBase()
        listaAlumno = db.leerTodos()
        db.close()

        // Inicializar la lista filtrada con todos los alumnos
        listaFiltrada = ArrayList(listaAlumno)

        adaptador = MiAdaptador(listaFiltrada, requireContext(), this)
        rcvLista.adapter = adaptador

        // Configurar el SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                filtrarLista(newText)
                return true
            }
        })

        // Agregar funcionalidad al botón btnNuevo
        btnNuevo.setOnClickListener {
            cambiarDBFragment()
        }

        return view
    }

    private fun filtrarLista(query: String?) {
        val queryText = query?.toLowerCase() ?: ""
        listaFiltrada.clear()

        if (queryText.isEmpty()) {
            listaFiltrada.addAll(listaAlumno)
        } else {
            listaFiltrada.addAll(
                listaAlumno.filter {
                    it.nombre.toLowerCase().contains(queryText) ||
                            it.matricula.toLowerCase().contains(queryText) ||
                            it.especialidad.toLowerCase().contains(queryText)
                }
            )
        }

        adaptador.notifyDataSetChanged()
    }

    private fun cambiarDBFragment() {
        parentFragmentManager.beginTransaction()
            .replace(R.id.frmContenedor, DbFragment())
            .addToBackStack(null)
            .commit()
    }

    override fun onItemClick(alumno: Alumno) {
        val bundle = Bundle().apply {
            putSerializable("mialumno", alumno)
        }

        val dbFragment = DbFragment().apply {
            arguments = bundle
        }

        parentFragmentManager.beginTransaction()
            .replace(R.id.frmContenedor, dbFragment)
            .addToBackStack(null)
            .commit()
    }
}
