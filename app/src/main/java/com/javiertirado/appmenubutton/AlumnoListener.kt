package com.javiertirado.appmenubutton

import com.javiertirado.appmenubutton.database.Alumno

interface AlumnoListener {
    fun onAlumnoAdded(alumno: Alumno)
}

