package com.javiertirado.appmenubutton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.widget.SearchView
import android.widget.Filter
import androidx.appcompat.app.AlertDialog
import com.example.appmenubutton.R

class ListFragment : Fragment() {

    private lateinit var listView: ListView
    private lateinit var searchView: SearchView
    private lateinit var arrayList: ArrayList<String>
    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        listView = view.findViewById(R.id.listAlumnos)
        searchView = view.findViewById(R.id.searchView)

        // Cargar los alumnos desde los recursos
        val items = resources.getStringArray(R.array.alumnos)
        arrayList = ArrayList(items.toList())

        // Adaptador para la lista con filtrado personalizado
        adapter = object : ArrayAdapter<String>(requireContext(), android.R.layout.simple_list_item_1, arrayList) {
            private var filteredItems: ArrayList<String> = ArrayList(arrayList)

            override fun getCount(): Int {
                return filteredItems.size
            }

            override fun getItem(position: Int): String? {
                return filteredItems[position]
            }

            override fun getFilter(): Filter {
                return object : Filter() {
                    override fun performFiltering(constraint: CharSequence?): FilterResults {
                        val charString = constraint?.toString() ?: ""
                        filteredItems = if (charString.isEmpty()) {
                            ArrayList(arrayList)
                        } else {
                            val filteredList = arrayList.filter {
                                it.contains(charString, ignoreCase = true)
                            } as ArrayList<String>
                            filteredList
                        }
                        return FilterResults().apply { values = filteredItems }
                    }

                    override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                        filteredItems = if (results?.values == null) {
                            ArrayList(arrayList)
                        } else {
                            results.values as ArrayList<String>
                        }
                        notifyDataSetChanged()
                    }
                }
            }
        }

        listView.adapter = adapter

        // Manejo de clic en elementos de la lista
        listView.setOnItemClickListener { parent, view, position, id ->
            val alumno: String = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Lista de Alumnos")
            builder.setMessage("$position: $alumno")
            builder.setPositiveButton("OK") { dialog, which -> }
            builder.show()
        }

        // Configurar el SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })

        return view
    }
}
