package com.javiertirado.appmenubutton

import java.io.Serializable

class AlumnoLista (
    var id: Int = 0,
    var matricula: String = "",
    var nombre: String = "",
    var domicilio: String = "",
    var especialidad: String = "",
    var foto: Int = 0, // Recurso de imagen drawable
    var fotoUri: String = "" // URI de la imagen seleccionada
): Serializable
