package com.javiertirado.appmenubutton

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmenubutton.R
import com.javiertirado.appmenubutton.database.Alumno
import com.google.android.material.imageview.ShapeableImageView

class MiAdaptador(
    private var listaAlumnos: List<Alumno>,
    private val context: Context,
    private val itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<MiAdaptador.ViewHolder>(), Filterable {

    private var filteredAlumnosList: List<Alumno> = listaAlumnos

    interface OnItemClickListener {
        fun onItemClick(alumno: Alumno)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.alumno_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(filteredAlumnosList[position], itemClickListener)
    }

    override fun getItemCount(): Int {
        return filteredAlumnosList.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val query = constraint?.toString()?.lowercase()?.trim() ?: ""
                filteredAlumnosList = if (query.isEmpty()) {
                    listaAlumnos
                } else {
                    listaAlumnos.filter {
                        it.nombre.lowercase().contains(query) || it.especialidad.lowercase().contains(query)
                    }
                }
                val filterResults = FilterResults()
                filterResults.values = filteredAlumnosList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredAlumnosList = results?.values as List<Alumno>? ?: listaAlumnos
                notifyDataSetChanged()
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val foto: ShapeableImageView = itemView.findViewById(R.id.foto)
        private val nombre: TextView = itemView.findViewById(R.id.txtAlumnoNombre)
        private val carrera: TextView = itemView.findViewById(R.id.txtCarrera)
        private val matricula: TextView = itemView.findViewById(R.id.txtMatricula)

        fun bind(alumno: Alumno, clickListener: OnItemClickListener) {
            nombre.text = alumno.nombre
            carrera.text = alumno.especialidad
            matricula.text = alumno.matricula

            // Load the photo using Glide
            Glide.with(context)
                .load(alumno.foto)
                .placeholder(R.drawable.baseline_account_24) // Show a placeholder image if the photoUrl is not available
                .error(R.drawable.baseline_account_24) // Show an error image if the load fails
                .into(foto)

            itemView.setOnClickListener {
                clickListener.onItemClick(alumno)
            }
        }
    }
}
