package com.javiertirado.appmenubutton

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import com.example.appmenubutton.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var buttonNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Verifica que el ID btnNavegador esté correctamente definido en activity_main.xml
        buttonNavigationView = findViewById(R.id.btnNavegador)
        buttonNavigationView.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.btnHome -> {
                    cambiarFrame(HomeFragment())
                    true
                }
                R.id.btnLista -> {
                    cambiarFrame(ListFragment())
                    true
                }
                R.id.btnDb -> {
                    cambiarFrame(DbFragment())
                    true
                }
                R.id.btnSalir -> {
                    cambiarFrame(SalirFragment())
                    true
                }
                else -> false
            }
        }

        // Configuración de los insets de la ventana para evitar problemas de padding
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        // Carga el fragmento por defecto
        if (savedInstanceState == null) {
            cambiarFrame(HomeFragment())
        }
    }

    private fun cambiarFrame(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.frmContenedor, fragment) // Verifica que frmContenedor esté correctamente definido en activity_main.xml
            .commit()
    }

}
